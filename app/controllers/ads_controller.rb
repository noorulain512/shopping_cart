class AdsController < ApplicationController
  before_action :set_ad, only: %i[ show edit update destroy ]
  def index
    @ads = current_admin.ad
  end
 
  def show
    @ad = Ad.find(params[:id])
  end

  def new 
    @ad = Ad.new
  end

  def edit
  end

  def create
    @ad = Ad.new(ad_params)
    @ad.admin_id = current_admin.id 
    respond_to do |format|
      if @ad.save
        format.html { redirect_to @ad, notice: "Ad was successfully created." }
        format.json { render :show, status: :created, location: @ad }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @ad.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @ad.update(ad_params)
        format.html { redirect_to @ad, notice: "Ad was successfully updated." }
        format.json { render :show, status: :ok, location: @ad }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ad.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @ad.destroy
    respond_to do |format|
      format.html { redirect_to ads_url, notice: "Ad was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
  def set_ad
    @ad = Ad.find(params[:id])
  end

  def ad_params
        params.require(:ad).permit(:title, :description, :price, :location, :mobileNumber, :admin_id, :category_id, :image)
  end
  
end

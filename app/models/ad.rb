class Ad < ApplicationRecord
 belongs_to :admin
 belongs_to :category
 has_one_attached :image;
end

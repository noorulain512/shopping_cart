class Admin < ApplicationRecord
   devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :validatable      
    has_many :ads
   has_one_attached :avatar
 end

json.extract! ad, :id, :title, :description, :price, :location, :mobileNumber, :admin_id, :category_id,:created_at, :updated_at
json.url ad_url(ad, format: :json)

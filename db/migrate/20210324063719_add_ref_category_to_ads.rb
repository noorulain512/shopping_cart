class AddRefCategoryToAds < ActiveRecord::Migration[6.1]
  def change
    add_foreign_key :ads, :categories
  end
end

class CreateAds < ActiveRecord::Migration[6.1]
  def change
    create_table :ads do |t|
      t.string :title
      t.string :description
      t.integer :admin_id
      t.float :price
      t.string :location
      
      t.timestamps
    end
  end
end
